Application-under-test: U_soljanka - a web-application for libraries, draft version.

Functional areas:
    Infrastructure
        1. Login

    Entities
        2. User
        3. Author
        4. Book
        5. Address

    Misc
        6. Calc
        7. About page

    RESTful Web Service
        8. VersionInfo resource
        9. Calculator resource
================================================================================
1. Login.
    1.1 There shall be a way to login as an administrator even when there are no user entries in the DB.
    1.2 Active users shall be able to login;
    1.3 Inactive users shall NOT be able to login;
    1.4 Invalid credentials shall be ignored. The user shall not be logged in and be warned about the issue;
    1.5 "Home" shall not be accessible for users not logged in;
    1.6 The user shall be able to log out.

2. "User" entity.
    2.1 There shall be "Users" page;
    2.2 "Users" page shall show total count of user entries (except admin);
    2.3 "Users" page shall show a list of all user entries (except admin);
    2.4 The system shall allow to create new user entry:
        2.4.1 It shall be possible to create a user entry from "Users" page;
        2.4.2 It shall be possible to create a user entry from Home page;
        2.4.3 New entry shall be inactive.
    2.5 The system shall allow to edit user entry;
    2.6 It shall be possible to activate/inactivate an entry;
    2.7 It shall be possible to cancel(without saving changes):
        2.7.1 ... entry creating;
        2.7.1 ... entry editing;
    2.8 The system shall allow to delete an entry.

3. "Author" entity.
    3.1 There shall be "Authors" page;
    3.2 "Authors" page shall show total count of entries;
    3.3 "Authors" page shall show a list of all entries;
    3.4 The system shall allow to create new entry;
        3.4.1 It shall be possible to create an entry from "Authors" page;
        3.4.2 It shall be possible to create an entry from Home page.
    3.5 The system shall allow to edit an entry;
    3.6 The system shall allow to delete an entry.
    3.7 It shall be possible to cancel(without saving changes):
        3.7.1 ... entry creating;
        3.7.2 ... entry editing.
        
4. "Book" entity.
    4.1 There shall be "Books" page;
    4.2 "Books" page shall show total count of entries;
    4.3 "Books" page shall show a list of all entries;
    4.4 The system shall allow to create new entry;
        4.4.1 It shall be possible to create an entry from "Books" page;
        4.4.2 It shall be possible to create an entry from Home page.
    4.5 The system shall allow to edit an entry;
    4.6 The system shall allow to delete an entry.
    4.7 It shall be possible to cancel(without saving changes):
        4.7.1 ... entry creating;
        4.7.2 ... entry editing.
    4.8 It shall be possible to specify a book's author.
        4.8.1 If there are no "author" entries in the system, then it shall be possible to specify at least "unknown" author name;
        4.8.2 All "author" entries shall be available for selecting upon creating/editing a book entry.
        
5. "Address" entity.
    5.1 There shall be "Addresses" page;
    5.2 "Addresses" page shall show total count of entries;
    5.3 "Addresses" page shall show a list of all entries;
    5.4 The system shall allow to create new entry;
        5.5.1 It shall be possible to create an entry from "Addresses" page;
        5.5.2 It shall be possible to create an entry from Home page.
    5.5 The system shall allow to edit an entry;
    5.6 The system shall allow to delete an entry.
    5.7 It shall be possible to cancel(without saving changes):
        5.7.1 ... entry creating;
        5.7.2 ... entry editing.
    5.8 It shall be possible to specify a who's an Address is - all "user" entries(except admin) shall be available for selecting upon creating/editing an address entry.

6. "Calc" page.
    6.1 The Calc shall support: +, -, x, / operations;
    6.2 The Calc shall allow to use 2 or three terms;
    6.3 Valid values range is [-169.13; 288.12].

7. "About" page.
    TBD.

8. VersionInfo REST WS resource
    8.1 Shall be accessible via HTTP GET on /rest/v1/version URL;
    8.2 Shall be provide description and version number info;

9. Calculator REST WS resource.
    9.1 Shall be accessible via HTTP GET on /rest/v1/calc URL;
    9.2 Shall support +, -, x, / operations. Operation to use shall be specified via "operation=" GET query parameter;
    9.3 Shall support 2 terms which shall be specified via "term1=" and "term2=" GET query parameters;
    9.4 Shall return HTTP_STATUS.OK upon successful calculation even if there boundaries violation;
    9.5 Shall not return HTTP_STATUS.OK upon missing query parameters, un-parsible values etc.

================================================================================
